# Installation Instructions

Make sure you have `node` and `yarn` installed.
You can install the why3find VSCode extension from the
the why3find root directory or from its vscode sub-directory.

From the why3find root directory:

    > make install-vscode

From the why3find/vscode directory:

    > make package
    > make install
