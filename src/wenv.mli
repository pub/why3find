(**************************************************************************)
(*                                                                        *)
(*  This file is part of the why3find.                                    *)
(*                                                                        *)
(*  Copyright (C) 2022-2024                                               *)
(*    CEA (Commissariat à l'énergie atomique et aux énergies              *)
(*         alternatives)                                                  *)
(*                                                                        *)
(*  you can redistribute it and/or modify it under the terms of the GNU   *)
(*  Lesser General Public License as published by the Free Software       *)
(*  Foundation, version 2.1.                                              *)
(*                                                                        *)
(*  It is distributed in the hope that it will be useful,                 *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *)
(*  GNU Lesser General Public License for more details.                   *)
(*                                                                        *)
(*  See the enclosed LICENSE.md for details.                              *)
(*                                                                        *)
(**************************************************************************)

(* -------------------------------------------------------------------------- *)
(* --- Why3 Environment                                                   --- *)
(* -------------------------------------------------------------------------- *)

val options :
  ?packages:bool ->
  ?provers:bool ->
  ?drivers:bool ->
  unit -> (string * Arg.spec * string) list

val arg1 : string -> string
val argv : string list -> string list
val argfiles : ?exts:string list -> string list -> string list
val allfiles : ?exts:string list -> (string -> unit) -> string -> unit

val default : Config.config

val load_raw_config : unit -> Json.t

val add_options : Config.config -> Config.config

val load_config : unit -> Config.config

val save : Config.config -> unit

val init : ?root:string -> ?loadplugins:bool -> Config.config -> Project.env

val update_why3_config : Why3.Whyconf.config -> unit
(** Update [~/.why3conf] with the specified number of jobs, if modified. *)
